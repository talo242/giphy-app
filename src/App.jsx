import React from 'react';
import Header from './components/Header';
import './App.css';
import GifList from './components/GifList';
import SearchBox from './components/Searchbox';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gifs: [],
      loading: true,
      search: '',
    }
  }

  componentDidMount() {
    const url = 'http://api.giphy.com/v1/gifs/trending?api_key=7YIJEiCrZjNszVSJeXD5JkzA9Ud7dJCG&limit=5';
    fetch(url).then(res => res.json()).then(response => {
      const { data } = response;

      this.setState({
        gifs: data,
        loading: false,
      })
    })
  }

  handleInputChange = (event) => {
    this.setState({
      search: event.target.value,
    });
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const query = this.state.search;
    if (query === '') return;

    const url = `http://api.giphy.com/v1/gifs/search?api_key=7YIJEiCrZjNszVSJeXD5JkzA9Ud7dJCG&q=${query}`

    fetch(url).then(res => res.json()).then(response => {
      const { data } = response;

      this.setState({
        gifs: data,
        loading: false,
        search: '',
      })
    })
  }

  getRandomGif = () => {
    const url = 'http://api.giphy.com/v1/gifs/random?api_key=7YIJEiCrZjNszVSJeXD5JkzA9Ud7dJCG';
    fetch(url).then(res => res.json()).then(jsonResponse => {
      // const id = jsonResponse.data.id;
      // const image_original_url = jsonResponse.data.image_original_url;
      // const status = jsonResponse.data.status;
      const { data } = jsonResponse;

      this.setState({
        gifs: this.state.gifs.concat([data]),
      })
    })
  }

  render() {
    return (
      <div className="App">
        <div className="container">
          <Header />
          <SearchBox
            value={this.state.search}
            onChange={this.handleInputChange}
            onSubmit={this.handleSubmit}
          />
          <GifList
            gifs={this.state.gifs}
            getRandomGif={this.getRandomGif}
          />
        </div>
      </div>
    );
  }
}

export default App;
