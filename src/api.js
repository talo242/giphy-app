const host = 'http://api.giphy.com';
const key = '7YIJEiCrZjNszVSJeXD5JkzA9Ud7dJCG';

const api = {
  trending: async function() {
    const response = await fetch(`${host}/v1/gifs/trending/?api_key=${key}&limit=5`).then(res => res.json()).then(response => {
      const { data } = response;
      return data;
    })

    return response;
  }
}

export default api;
