import React from 'react';

function Nav(props) {
  return (
    <ul>
      {
        props.list && props.list.map(elemento => (
          <li className="link-item">
            <a href={elemento.url}>
              {elemento.title}
            </a>
          </li>
        ))
      }
    </ul>
  );
}

export default Nav;
