import React from 'react';

function Content() {
  return (
    <div>
      <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Explicabo omnis porro minima ullam atque animi blanditiis qui tenetur nobis, pariatur ipsa in facere? Natus maiores molestiae suscipit aperiam. Deleniti, architecto?</p>
      <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Explicabo omnis porro minima ullam atque animi blanditiis qui tenetur nobis, pariatur ipsa in facere? Natus maiores molestiae suscipit aperiam. Deleniti, architecto?</p>
      <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Explicabo omnis porro minima ullam atque animi blanditiis qui tenetur nobis, pariatur ipsa in facere? Natus maiores molestiae suscipit aperiam. Deleniti, architecto?</p>
    </div>
  );
}

export default Content;
