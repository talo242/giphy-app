import React from 'react';

function Header(props) {
  return (
    <div className="header">
      <h1 className="logo">GIPHY</h1>
      <div className="buttons-container">
        <button className="header-button">Upload</button>
        <button className="header-button">Create</button>
      </div>
    </div>
  );
}

export default Header;
